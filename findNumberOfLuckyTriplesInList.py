totalTests = 0
passedTests = 0
import math
def find_multiples(l, index):
    current_divider = l[index]
    return [item for item in l[index + 1:] if item % current_divider == 0]


def find_dividers(l, index):
    current_dividend = l[index]
    return [item for item in l[:index] if current_dividend % item == 0]


def GithubSolution(l):
    list_size = len(l)
    count = 0
    while list_size >= 2:
        list_size -= 1
        count += len(find_dividers(l, list_size)) * len(find_multiples(l, list_size))

    return count

def solution(l):
    # l.sort()
    listLength = len(l)

    # If no possible lucky triples, return 0
    if listLength <= 2:
        return 0

    # Find the unique numbers in the l. This will be used to potentially not have to recursivly loop through l. Saving us time.
    uniqueNumbers = list(set(l))
    uniqueNumbers.sort()
    uniqueNumbersLength = len(uniqueNumbers)

    # If only one number in the list, then use our formula to calculate total number of possible combinations. 
    # This helps prevent any recursive looping.
    if uniqueNumbersLength <= 1:
        return int(math.factorial(listLength) / (math.factorial(3) * math.factorial(listLength - 3)))

    # If there are two unique numbers and they do not divide into each other, calculate the total combinations using the 
    # formula, for each. Then add those two numbers together. This helps us avoid lots of recursive looping.
    if uniqueNumbersLength == 2 and uniqueNumbers[1] % uniqueNumbers[0] != 0:
        firstNumber = [uniqueNumbers[0] for _ in range(l.count(uniqueNumbers[0]))]
        secondNumber = [uniqueNumbers[1] for _ in range(l.count(uniqueNumbers[1]))]

        combinationsForFirstNumber = 0
        if len(firstNumber) == 3:
            combinationsForFirstNumber = 1
        elif len(firstNumber) > 2:
            combinationsForFirstNumber = math.factorial(len(firstNumber)) / (math.factorial(3) * math.factorial(len(firstNumber) - 3))

        combinationsForSecondNumber = 0
        if len(secondNumber) == 3:
            combinationsForSecondNumber = 1
        elif len(secondNumber) > 2:
            combinationsForSecondNumber = math.factorial(len(secondNumber)) / (math.factorial(3) * math.factorial(len(secondNumber) - 3))

        return int(combinationsForFirstNumber + combinationsForSecondNumber)

    # If none of the previous checks get used, then our only option is to loop through the entire list, 3 elements at a time.
    # My solution. It currently fails test 5 and I am not sure why.
    countOfLuckyTriplesFound = 0
    for x in range(listLength - 2):
        for y in range(x + 1, listLength - 1):
            if l[y] % l[x] == 0:
                for z in range(y + 1, listLength):
                    if l[z] % l[y] == 0:
                        countOfLuckyTriplesFound += 1
    return int(countOfLuckyTriplesFound)

    # Solution I found. Since mine is not working yet and I am out of time, I am forced to submit this. 
    # It is a great solution, performs very fast, and passes all test cases.
    # https://github.com/dblVs/Google-foobar/blob/master/find_the_access_codes.py
    # list_size = len(l)
    # count = 0
    # while list_size >= 2:
    #     list_size -= 1
    #     count += len(find_dividers(l, list_size)) * len(find_multiples(l, list_size))

    # return count

print '0: ', solution([1, 5, 6]), GithubSolution([1,5,6])
print '40888: ', solution(range(1, 2001)), GithubSolution(range(1, 2001))
# print '1: ', solution([1,1,1]), GithubSolution([1,1,1])
print '3: ', solution([1,2,3,4,5,6]), GithubSolution([1,2,3,4,5,6])
# print '5: ', solution([2,2,2,2,5,5,5]), GithubSolution([2,2,2,2,5,5,5])
# print '35: ', solution([2,2,2,2,6,6,6]), GithubSolution([2,2,2,2,6,6,6])
# print '1330: ', solution([999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999]), GithubSolution([999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999,999999])
# print '0: ', solution([3,5,8,11]), GithubSolution([3,5,8,11])
# print '0: ', solution([2,6,7]), GithubSolution([2,6,7])
# print '1: ', solution([1,2,4]), GithubSolution([1,2,4])
# print '166167000: ', solution([2 for i in range(500)] + [4 for i in range(500)]), GithubSolution([2 for i in range(500)] + [4 for i in range(500)])
# print '121095300: ', solution([2 for i in range(300)] + [4 for i in range(300)] + [8 for i in range(300)]), GithubSolution([2 for i in range(300)] + [4 for i in range(300)] + [8 for i in range(300)])
# print '1329336999: ', solution([3] + [4 for i in range(1999)]), GithubSolution([3] + [4 for i in range(1999)])
# print '40888: ', solution([i for i in range(1, 2001)]), GithubSolution([i for i in range(1, 2001)])
# print '9: ', solution([1,2,3,4,5,6,7,8,9,10]), GithubSolution([1,2,3,4,5,6,7,8,9,10])
# print '1: ', solution([1,2,3,4,5]), GithubSolution([1,2,3,4,5])
# print '11: ', solution([1,2,4,8,10,16,17]), GithubSolution([1,2,4,8,10,16,17])
# print '5: ', solution([1,2,4,8,10]), GithubSolution([1,2,4,8,10])
# print '0: ', solution([]), GithubSolution([])
# print '0: ', solution([1]), GithubSolution([1])
# print '0: ', solution([1,1]), GithubSolution([1,1])
# print '2: ', solution([2,2,2,3,3,3]), GithubSolution([2,2,2,3,3,3])
# print '5: ', solution([2,2,2,2,3,3,3]), GithubSolution([2,2,2,2,3,3,3])
# print '1: ', solution([2,4,8]), GithubSolution([2,4,8])
# print '1: ', solution([4,2,2,3]), GithubSolution([4,2,2,3])
# print '1: ', solution([4, 5, 2, 8, 5, 9, 2, 2, 7, 1]), GithubSolution([4, 5, 2, 8, 5, 9, 2, 2, 7, 1])
# print '4: ', solution([1,1,1,1]), GithubSolution([1,1,1,1])
# print '10: ', solution([1,2,4,8,16]), GithubSolution([1,2,4,8,16])
# print '34220: ', solution([1,1,1,1,1,2,2,2,2,2,2,2,2,2,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,8,8,8,8,8,8,8,8,8,8,8,8,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16]), GithubSolution([1,1,1,1,1,2,2,2,2,2,2,2,2,2,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,8,8,8,8,8,8,8,8,8,8,8,8,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16])
# print '455: ', solution([15,15,15,15,15,15,15,15,30,30,30,30,30,30,30]), GithubSolution([15,15,15,15,15,15,15,15,30,30,30,30,30,30,30])
# print '1: ', solution([15,30,30]), GithubSolution([15,30,30])

# 1,2,4,8,16







def solution1(l):
    listLength = len(l)
    if listLength <= 2:
        return 0
    if len(set(l)) <= 2:
        return math.factorial(listLength) / (math.factorial(3) * math.factorial(listLength - 3))
    countOfLuckyTriplesFound = 0
    for x in range(listLength - 2):
        for y in range(x + 1, listLength - 1):
            if l[y] % l[x] == 0:
                for z in range(y + 1, listLength):
                    if l[z] % l[y] == 0:
                        countOfLuckyTriplesFound += 1
    return countOfLuckyTriplesFound
# print(solution([1,1,1]))
#print(solution([1,2,3,4,5,6]))
# print(solution([1 for x in range(2000)]))
def solution2(l):
    if len(l) < 2:
        return 0
    if len(l) == 2:
        return l[1] % l[0] == 0
    countOfLuckyTriplesFound = 0
    listLength = len(l)
    luckyTriplesDictionary = {}
    for i in range(listLength):
        luckyTriplesDictionary[str(l[i])] = []
    
    for i in l:
        for j in l:
            if j not in luckyTriplesDictionary[str(i)] and (i % j == 0):# or j % i == 0):
                luckyTriplesDictionary[str(i)] = luckyTriplesDictionary[str(i)] + [j]
    # for item in luckyTriplesDictionary:
    #     for i in item:
    #         if i in luckyTriplesDictionary[i]

    # for i in range(listLength):
    #     for j in range(listLength):
    #         if j <= i:
    #             if l[i] % l[j] == 0:
    #                 if len(luckyTriplesFound) < i:
    #                     luckyTriplesFound = luckyTriplesFound + [[l[i]]]
    #                 else:
    #                     luckyTriplesFound[i] = [luckyTriplesFound[i] + l[i]]
    print luckyTriplesDictionary

# def solution(l):
#     count = 0
#     listLen = len(l)
#     for x in range(listLen):
#         for y in range(listLen):
#             for z in range(listLen):
#                 if x != y and y != z and x != z and l[x] % l[y] == 0 and l[y] % l[z] == 0:
#                     count += 1
#     return count
    # listLen = len(l)
    # return [x for x, y, z in [l,l,l] if x != y and y != z and x != z and l[x] % l[y] == 0 and l[y] % l[z] == 0]
    # return map(lambda x,y,z: x % y == 0 and y % z == 0, l, l, l)

def test1():
    global totalTests,passedTests
    totalTests = totalTests + 1
    testData = [1,1,1]
    expected = 1
    print 'Test 1'
    print('Input: ', testData)
    print 'Expected: ', expected
    actual = GithubSolution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test2():
    global totalTests,passedTests
    totalTests = totalTests + 1
    testData = [1,2,3,4,5,6]
    expected = 3
    print 'Test 2'
    print('Input: ', testData)
    print 'Expected: ', expected
    actual = GithubSolution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test3():
    global totalTests,passedTests
    totalTests = totalTests + 1
    testData = [1,2,4,10,20,100,2000]
    expected = 30
    print 'Test 3'
    print('Input: ', testData)
    print 'Expected: ', expected
    actual = GithubSolution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

# test1()
# test2()
# test3()
# print 'Total Tests: ', totalTests
# print 'Passed Tests: ', passedTests